package be.hics.sandbox.sumdigpower;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.stream.Collectors;

@SpringBootApplication
public class SumDigPowerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SumDigPowerApplication.class, args);
        System.out.println(SumDigPower.sumDigPow(1,10000).stream().map(l -> Long.toString(l)).collect(Collectors.joining(", ")));
	}
}
